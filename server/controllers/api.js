var cheerio  = require('cheerio');
var fs       = require('fs');
var path       = require('path');
var request  = require('request');

//Import Item Model
var Item = require('../models/item.js');


///--------------
var mainPage             = 'http://www.pathofexile.com/';
var standardTradingForum = 'http://www.pathofexile.com/forum/view-forum/standard-trading-shops';

var parseShopThread = function(html){

  //Find script that contains item info, match it and eval it to convert it to an array.
  var itemExpression = new RegExp(/function\(R\) { \(new R\((.*)\)\).run\(\); \}\);/);
  var matchedItemEx = html.match(itemExpression);

  if(matchedItemEx[1]){
    var parsedItems = eval(matchedItemEx[1]);
        
    //for(var i = 0; i < parsedItems.length; i++){            
    for(var i = 0; i < 2; i++){          

        //Get item, create new item model, save  
        var itemInfo = parsedItems[i][1];
        var itemModel = new Item(itemInfo).save();
    }

    return true;

  }

  return false;
}

var parseShopPage = function(html, type){

  //Parse main shop table, find all threads, except the first two stickies
  var numStickies = type || 2;
  var $ = cheerio.load(html);
  
  var shopTable = $('#view_forum_table');
  var shopThreads  = shopTable.find('tr td.thread .thread_title .title a');
  var shopThreadUrls = [];

  shopThreads.each(function(){
    shopThreadUrls.push(mainPage + $(this).attr('href'));
  });

  //Remove stickied
  shopThreadUrls.splice(0, numStickies);

  return shopThreadUrls;
}


//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

//Set up api methods to call
exports.parseShop = function(req, res) {

  fs.readFile('./server/shop.txt', 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }

    var threadUrls = parseShopPage(data);
    
    res.send(JSON.stringify(threadUrls));
  });

  //var itemTest = new Item({name: 'testing'});

};

exports.parseThread = function(req, res){

  fs.readFile('./server/shopthread.txt', 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }

    var newItems = parseShopThread(data);

    res.send(JSON.stringify(newItems));
  });


};

exports.post = function(req, res) {

    //parse item here
    //new Thread(/*parsed item here*/).save();
}
 
exports.list = function(req, res) {
  /*Thread.find(function(err, threads) {
    res.send(threads);
  });*/
}
 
// first locates a thread by title, then locates the replies by thread ID.
exports.show = (function(req, res) {
    /*Thread.findOne({title: req.params.title}, function(error, thread) {
        var posts = Post.find({thread: thread._id}, function(error, posts) {
          res.send([{thread: thread, posts: posts}]);
        });
    })*/
});

/*

ONCE OFFLINE IS WORKING:

request(standardTradingForum, function (error, response, html) {
  if (!error && response.statusCode == 200) {

    parseShopPage(html);

  }
});


*/