/*

	EXAMPLE:

	~b\/o [0-9]+ chaos


	div.spoiler each spoiler

	#item-fragment-0
				   ^ --- position in config array




*/

var currencyNames = [
	{"Chromatic Orb"         : "chrom"},
	{"Orb of Alteration"     : "alt"},
	{"Jeweller's Orb"        : "jewel"},
	{"Orb of Chance"         : "chance"},
	{"Cartographer's Chisel" : "chisel"},
	{"Orb of Fusing"         : "fuse"},
	{"Orb of Alchemy"        : "alch"},
	{"Orb of Scouring"       : "scour"},
	{"Blessed Orb"           : "blessed"},
	{"Chaos Orb"             : "chaos"},
	{"Orb of Regret"         : "regret"},
	{"Regal Orb"             : "regal"},
	{"Gemcutter's Prism"     : "gcp"},
	{"Divine Orb"            : "divine"},
	{"Exalted Orb"           : "exa"},
	{"Vaal Orb"              : "vaal"}
];

var currencyAbbreviations = [
	"chrom",
	"alt",
	"jewel",
	"chance",
	"chisel",
	"fuse",
	"alch",
	"scour",
	"blessed",
	"chaos",
	"regret",
	"regal",
	"gcp",
	"divine",
	"exa",
	"vaal"
];

