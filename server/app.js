// The main application script, ties everything together.
 
var express = require('express');

var express = require('express')
var http = require('http');
var mongoose = require('mongoose');

var app = express();

// connect to Mongo when the app initializes
mongoose.connect('mongodb://localhost/test');

 
// set up the RESTful API, handler methods are defined in api.js
var api = require('./controllers/api.js');
/*app.post('/thread', api.post);
app.get('/thread/:title.:format?', api.show);*/

app.get('/parseShop'  , api.parseShop);
app.get('/parseThread', api.parseThread);

 
app.listen(3000);
console.log("Express server listening on port 3000");