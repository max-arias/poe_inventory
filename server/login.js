var request = require("request");
var cheerio  = require('cheerio');
var fs = require("fs");

var loginUrl       = "https://www.pathofexile.com/login";

var charactersUrl  = "http://www.pathofexile.com/character-window/get-characters";
var characterItems = "http://www.pathofexile.com/character-window/get-items";       // {character:"<name>"}
var characterStash = "http://www.pathofexile.com/character-window/get-stash-items"; // {league:Standard, tabs:1, tabIndex:0}

//TODO: Look into Redis

var requestLoginPage = function(callback){
    request(loginUrl, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        callback(body);       
      }
    }); 
};

function parse_cookies(_cookies) {
    var cookies = {};
 
    _cookies && _cookies.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        cookies[ parts[ 0 ].trim() ] = ( parts[ 1 ] || '' ).trim();
    });
/*
    "set-cookie": [
        "PHPSESSID=***********; path=/; HttpOnly"
    ],

*/
 
    return cookies;
}

var loginCallback = function(body){
    var $ = cheerio.load(body);      
    var loginHash = $('#hash').val();

    var login_obj = {
        "login_email"    : "USERNAME HERE",
        "login_password" : "PASSHERE",
        "remember_me"    : 0, 
        "hash"           : loginHash,
        "login"          : "Login"
    }

    request({
      uri: loginUrl,
      method: "POST",
      form: login_obj
    }, function(error, response, body) {

      fs.writeFile("error.js", error); 
      fs.writeFile("body.js", body); 
      fs.writeFile("response.js", JSON.stringify(response, null, 4)); 

      console.log(response.headers); //<-- This contains the PHPSESSID cookie
    });
};

requestLoginPage(loginCallback);


/*
request({
  uri: loginUrl,
  method: "POST",
  form: {
    name: "Bob"
  }
}, function(error, response, body) {
  console.log(body);
});
*/